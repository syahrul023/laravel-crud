
@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="card">
      <div class="card-header">
        <h4>Create New Barang</h4>
      </div>
      <div class="card-body">
        <form method="post" action="{{ URL('barang/store')}}">
          {{ csrf_field() }}
          <div class="form-group">
            <label>Nama</label>
            <input type="text" name="name" required="required" class="form-control">
          </div>
          <div class="form-group">
            <label>Harga</label>
            <input type="number" name="harga" required="required" class="form-control">
          </div>
          <div class="form-group">
            <label>Kategori</label>
            <input type="text" name="kategori" required="required" class="form-control">
          </div>
          <div class="form-group">
            <label>Stock</label>
            <input type="number" name="stok" required="required" class="form-control">
          </div>
          <button value="save" class="btn btn-primary">Simpan</button>
          <a href="{{URL('barang')}}" class="btn btn-danger">Batal</a>
        </form>
      </div>
    </div>
  </div>              
@endsection
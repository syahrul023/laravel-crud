
@extends('layouts.app')

@section('content')
	<div class="card-header text-center">Edit Barang</div>
              <div class="col-md-9 offset-md-3">
                <form action="{{ URL('barang/update')}}" method="post">
                  {{ csrf_field() }}
                  <div class="col-md-9">
                    <div class="form-group">
                      <input type="hidden" value="{{$m->id}}" name="id">
                      <label class="text blue"><b>Nama</b></label>
                      <input class="input border form-control" name="name" value="{{$m->name}}" type="text" required="required">
                    </div>
                      <div class="form-group">
                        <label class="text blue"><b>Harga</b></label>
                        <input type="number" class="input border form-control" value="{{$m->harga}}" name="harga">
                      </div>
                      <div class="form-group">
                        <label class="text blue"><b>Kategori</b></label>
                        <input type="text" class="input border form-control" value="{{$m->kategori}}" name="kategori">
                      </div>
                      <div class="form-group">
                        <label class="text blue"><b>Stock</b></label>
                        <input type="number" class="input border form-control" value="{{$m->stok}}" name="stok">
                      </div>                        
                        <input type="submit" class="btn btn-primary" value="save">
                        <a onclick="return confirm('Are You Sure To Cancel?')" href="{{URL('barang')}}" class="btn btn-danger">Cancel
                        </a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            @endsection
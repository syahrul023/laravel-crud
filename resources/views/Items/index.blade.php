@extends('layouts.app')

@section('content')

<div class="container">
  <div class="form-group">
      <a href="/items/buat" class="btn btn-success" >Create New Items</a>
  </div>
<table class="table">
                <thead style="background-color: navy; color: white;" >
                  <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Action</th>
                  </tr>
                </thead>
                @foreach($p as $key => $t)
                <tbody>
                  <tr>
                    <td>{{$key+1}}</td>                   
                    <td>{{$t->title}}</td>                   
                    <td align="left">{{$t->description}}</td>
                    <td>
                      <div class="btn-group">
                        <a href="{{URL('items/show/'.$t->id)}}" class="btn btn-primary">Show</a>
                        <a onclick="return confirm('Do You Want To Edit Items {{$t->title}} ??')" href="{{URL('mengedit/data/'.$t->id)}}" class="btn btn-warning">Edit</a>
                        <a onclick="return confirm('Do You Want To Delete Items {{$t->title}} ??')" href="{{URL('destroy/'.$t->id)}}" class="btn btn-danger">⛔Delete</a>
                      </div>
                    </td>
                  </tr>
                </tbody>
                @endforeach
              </table>
</div>

@endsection
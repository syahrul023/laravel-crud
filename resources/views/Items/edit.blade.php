
@extends('layouts.app')

@section('content')
	<div class="card-header text-center">EDIT Items</div>
              <div class="col-md-9 offset-md-3">
                <form action="{{ URL('update/items')}}" method="post">
                  {{ csrf_field() }}
                  <div class="col-md-9">
                    <div class="form-group">
                      <input type="hidden" value="{{$m->id}}" name="id">
                      <label class="text blue"><b>Title</b></label>
                      <input class="input border form-control" name="title" value="{{$m->title}}" type="text" required="required">
                      <div class="form-group">
                        <label class="text blue"><b>Description</b></label>
                        <input type="text" class="input border form-control" value="{{$m->description}}" name="description">
                        <br/>                        
                        <input type="submit" class="btn btn-primary" value="save">
                        <a onclick="return confirm('Are You Sure To Cancel?')" href="{{URL('items')}}" class="btn btn-danger">Cancel
                        </a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            @endsection